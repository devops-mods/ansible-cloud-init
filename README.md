# Ansible Cloud Init

Ansible playbook to build debian 10 with cloud init for proxmox


## How to install
### Create virtual env
```bash
python -m virtualenv ansible_virtualenv
source ansible_virtualenv/bin/activate 
```

### Install requirements
```bash
python -m pip install -r ./requirements.txt
```


## How to use

### Create vault file with default password
```bash
ansible-vault create group_vars/vault
```
Add into vault file the following lines, save and exit
```text
vault_pass: default_user_password
vault_become_pass: default_root_password
```

Add hosts in inventory file. ex:
```ansible
all: # keys must be unique, i.e. only one 'hosts' per group
    hosts:
        127.0.0.1:

```

### Proxmox

Into proxmox add cloud-init drive

### Execute playbook
```bash
ansible-playbook main.yml -i inventory.yml --ask-vault-password --extra-vars '@group_vars/vault'
```
