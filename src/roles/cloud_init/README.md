Role Name
=========

- Cloud init, install package, enable services, remove machine-id and disable default users

Requirements
------------

- no requirements

Role Variables
--------------

- No default variable is needed

Dependencies
------------

- No dependencies

Example Playbook
----------------



```ansible


- name: Playbook to install cloud init
  hosts: all

  pre_tasks:
    - debug:
        msg: 'Start to install cloud init'

  roles:
    - cloud_init

  post_tasks:
    - debug:
        msg: 'Cloud init is now installed'

```

License
-------

BSD

Author Information
------------------
